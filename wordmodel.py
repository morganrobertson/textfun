from collections import Counter
from itertools import groupby
import random

from markovchain import MarkovChain
from util import choice
import words


class NGramModel:
    def __init__(self, n=1):
        '''
        Initialize the model.

        Args:
            n: The number of words to use for predicting the next word.
        '''

        self._model = MarkovChain()
        self._n = n

    def build(self, lines, stripchars=None, sep=None, minCount=1):
        '''
        Build the model using the given text.

        Args:
            lines: An iterable containing strings or lines of text (e.g. a file
                handle).
            stripchars: A string containing characters to be stripped from the
                beginning or end of words. If not given, whitespace is
                stripped.
            sep: The delimiter string to use for splitting lines into words. If
                not given, whitespace is used.
            minCount: The minimum number of times an n-gram must appear to be
                included in the model.
        '''

        ngrams = words.ngrams(self._n + 1, lines, stripchars, sep)
        counts = Counter(ngrams)

        sort = sorted(counts.keys())
        for stem, transitions in groupby(sort, key=lambda x: x[:self._n]):
            for transition in transitions:
                count = counts[transition]

                if count >= minCount:
                    self._model.add_transition(stem, transition[1:], count)

    def build_from_file(self, filename, encoding='utf-8', stripchars=None,
                        sep=None, minCount=1):
        '''
        Build the model using text from a file.

        Args:
            filename: The name of the file to obtain text from.
            encoding: The encoding of the file.
            stripchars: A string containing characters to be stripped from the
                beginning or end of words. If not given, whitespace is
                stripped.
            sep: The delimiter string to use for splitting lines into words. If
                not given, whitespace is used.
            minCount: The minimum number of times an n-gram must appear to be
                included in the model.
        '''

        with open(filename, 'r', encoding=encoding) as f:
            self.build(f, stripchars, sep, minCount)

    def build_from_files(self, filenames, encoding='utf-8', stripchars=None,
                         sep=None, minCount=1):
        '''
        Build the model using text from multiple files.

        Args:
            filenames: An iterable of filenames to obtain text from.
            encoding: The encoding of the file.
            stripchars: A string containing characters to be stripped from the
                beginning or end of words. If not given, whitespace is
                stripped.
            sep: The delimiter string to use for splitting lines into words. If
                not given, whitespace is used.
            minCount: The minimum number of times an n-gram must appear to be
                included in the model.
        '''

        def lines():
            for filename in filenames:
                with open(filename, 'r', encoding=encoding) as f:
                    yield from f

        self.build(lines(), stripchars, sep, minCount)

    def predict_next_word(self, words, nPredictions=1):
        '''
        Predict the next word given the previous n words.

        Args:
            words: A sequence of n words used to predict the next word.
            nPredictions: The maximum length of the returned list of
                predictions.

        Returns:
            A list of the most likely words.
        '''

        words = tuple(word.lower() for word in words)
        predictions = self._model.most_likely(words, nPredictions)
        return [prediction[-1] for prediction in predictions]

    def generate_words(self, length, start=None):
        '''
        Generate a random sequence of words using the model.

        Args:
            length: The total length of the generated sequence.
            start: A sequence of words to start from.

        Yields:
            The next word in the sequence.
        '''

        if start is None:
            start = choice(self._model)

        for ngram in self._model.run(start, maxSteps=length):
            yield ngram[-1]
