from collections import deque
import string


def words(lines, stripchars=None, sep=None):
    '''
    Parse the words from a set of strings.

    Args:
        lines: An iterable containing strings or lines of text (e.g. a file
            handle).
        stripchars: A string containing characters to be stripped from the
            beginning or end of words. If not given, whitespace is stripped.
        sep: The delimiter string to use for splitting lines into words. If not
            given, whitespace is used.

    Yields:
        The next word.
    '''

    if stripchars is None:
        stripchars = string.whitespace + string.punctuation + string.digits

    for line in lines:
        for word in line.lower().split(sep):
            word = word.strip(stripchars)
            if len(word) > 0:
                yield word


def ngrams(n, lines, stripchars=None, sep=None):
    '''
    Parse the n-grams from a set of strings.

    To obtain n-grams, a sliding window of size n is used. Each time an n-gram
    is yielded, the window moves to the right by one word.

    Args:
        n: The number of words that each n-grams consists of.
        lines: An iterable containing strings or lines of text (e.g. a file
            handle).
        stripchars: A string containing characters to be stripped from the
            beginning or end of words. If not given, whitespace is stripped.
        sep: The delimiter string to use for splitting lines into words. If not
            given, whitespace is used.

    Yields:
        The next n-gram.
    '''

    ngram = deque(maxlen=n)

    for word in words(lines, stripchars, sep):
        ngram.append(word)

        if len(ngram) == n:
            yield tuple(ngram)
