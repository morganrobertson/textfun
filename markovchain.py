from util import choice


class MarkovChain:
    def __init__(self):
        self._transitions = {}

    def __iter__(self):
        return iter(self._transitions)

    def __len__(self):
        return len(self._transitions)

    def add_node(self, node):
        '''
        Add a node to the chain.

        Args:
            node: A hashable object to add as a node.
        '''

        if node not in self._transitions:
            self._transitions[node] = {}

    def add_transition(self, node0, node1, weight):
        '''
        Add a transition to the chain. If the specified nodes are not already
        in the chain, they are added. A node can be any hashable object.

        Args:
            node0: The node to start at.
            node1: The successor to node0.
            weight: The weight value of the transition. This can either be a
                probability or an unnormalized weight.
        '''

        if node0 not in self._transitions:
            self._transitions[node0] = {}

        if node1 not in self._transitions:
            self._transitions[node1] = {}

        self._transitions[node0][node1] = weight

    def most_likely(self, node, n=None, getWeights=False):
        '''
        Get the most likely successors of a node.

        Args:
            node: The node to get successors of.
            n: The maximum number of successors to return. If not given, all
                successors are returned.
            getWeights: If True, a weight is returned with each successor. In
                this case, the returned list consists of 2-tuples where the
                first item is the successor and the second item is the weight.

        Returns:
            A list of successors ordered from most likely to least likely.
        '''

        transitions = self._transitions[node]

        if getWeights:
            sort = sorted(transitions.items(), key=lambda x: x[1],
                          reverse=True)
        else:
            sort = sorted(transitions.keys(), key=lambda x: transitions[x],
                          reverse=True)

        return sort if n is None else sort[:n]

    def run(self, startState, stopState=None, maxSteps=None):
        '''
        Run the Markov chain until the specified stop state is reached or until
        the maximum number of steps is taken. If neither is specified, the
        chain generates states indefinitely.

        Args:
            startState: The state to start at.
            stopState: A state to stop at (optional).
            maxSteps: The maximum number of steps to take before stopping
                (optional).

        Yields:
            The current state after each step.
        '''

        currentState = startState
        steps = 0

        while currentState != stopState and steps != maxSteps:
            transitions = self._transitions[currentState]
            currentState = choice(transitions.keys(), transitions.values())
            steps += 1
            yield currentState
