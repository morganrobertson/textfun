import random


def choice(items, weights=None):
    '''
    Select an item with probability proportional to its relative weight.

    Args:
        items: A sequence of items to select from.
        weights: A sequence of weights, each of which corresponds to an item.
            If not given, uniform weights are used.

    Returns: A randomly selected item.
    '''

    if weights is None:
        weights = [1 for i in range(len(items))]

    rand = random.uniform(0, sum(weights))
    total = 0

    for item, weight in zip(items, weights):
        total += weight

        if rand <= total:
            return item
